# KP Assessment

This project is a technical test conducted as part of a job interview. The purpose was to create a web interface to find a company thanks to its business name or its Siret number. The information was retrieved from the Sirène API of the French Government. https://entreprise.data.gouv.fr

## Technologies & ressources

- Vue.js 2 : https://vuejs.org
- Bootstrap 5.1 : https://getbootstrap.com/ 
- Boostrap Vue : https://bootstrap-vue.org
- Api Sirène : https://entreprise.data.gouv.fr
- Icons : https://www.flaticon.com/

## How to run this project

### Install dependencies
```
$ npm install
```

### Compiles and run local serve for development
```
$ npm run serve
```
