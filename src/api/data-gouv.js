import axios from "axios"

const URL_API = "https://entreprise.data.gouv.fr/api/sirene/v1";

function getCompanyByName(name, index_page = 1, per_page= 10) {
    return new Promise((resolve, reject) => {
        const url = `${URL_API}/full_text/${name}?per_page=${per_page}&page=${index_page}`;
        axios.get(url)
            .then((response) => {
                let data = {};
                if (response.status === 200 && response.data) {
                    data.companies = (response.data.etablissement) ? response.data.etablissement : [];
                    data.total_pages = (response.data.total_pages) ? response.data.total_pages : 0;
                    data.total_results = (response.data.total_results) ? response.data.total_results : 0;
                    data.updated_at = Date.now();
                }
                resolve(data);
            })
            .catch((error) => {
                if (error.response && error.response.status === 404) {
                    reject({status: 404, message: "Aucun résultat trouvé !"});
                }
                reject(error);
            })
    });

}

function getCompanyBySiret(siret, index_page = 1, per_page = 10) {
    return new Promise((resolve, reject) => {
        const url = `${URL_API}/siret/${siret}?per_page=${per_page}&page=${index_page}`;
        axios.get(url)
            .then((response) => {
                let data = {};
                if (response.status === 200 && response.data) {
                    data.companies = (response.data.etablissement) ? [response.data.etablissement] : [];
                    data.total_pages = 1;
                    data.total_results = 1;
                    data.updated_at = Date.now();
                }
                resolve(data);
            })
            .catch((error) => {
                if (error.response && error.response.status === 404) {
                    reject({status: 404, message: "Aucun résultat trouvé !"});
                }
                reject(error);
            })
    });
}

export {getCompanyByName, getCompanyBySiret}